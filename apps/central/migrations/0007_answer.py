# Generated by Django 2.2 on 2019-04-19 02:04

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('central', '0006_auto_20190419_0420'),
    ]

    operations = [
        migrations.CreateModel(
            name='Answer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.CharField(max_length=120)),
                ('right', models.BooleanField(default=False)),
                ('questions', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='central.Question')),
            ],
        ),
    ]
