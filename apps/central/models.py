from django.db import models
from django.contrib.auth.models import User


class Topic(models.Model):

    title = models.CharField(max_length=128)
    link = models.URLField(null=True, blank=True)
    video = models.CharField(max_length=256, blank=True, null=True)
    gif = models.CharField(max_length=256, blank=True, null=True)
    text = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    show = models.BooleanField(default=True)


class Test(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    topic = models.ForeignKey('Topic', on_delete=models.CASCADE, blank=True, null=True)


class Question(models.Model):

    text = models.TextField()
    test = models.ForeignKey('Test', on_delete=models.CASCADE)


class Answer(models.Model):

    text = models.CharField(max_length=120)
    right = models.BooleanField(default=False)
    questions = models.ForeignKey('Question', on_delete=models.CASCADE)


class Result(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    topic = models.ForeignKey('Topic', on_delete=models.CASCADE, blank=True, null=True)
    right_answers = models.PositiveIntegerField(default=0)
