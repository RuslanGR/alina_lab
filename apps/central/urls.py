# from django.views.generic import TemplateView
from django.urls import path

from .views import TopicsList, TopicView, TestView


app_name = 'central'
urlpatterns = (
    path('', TopicsList.as_view(), name='index'),
    path('topic/<int:pk>/', TopicView.as_view(), name='topic'),
    path('test/<int:pk>/', TestView.as_view(), name='test'),
)
