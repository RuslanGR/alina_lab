from django.apps import AppConfig


class CentralConfig(AppConfig):
    name = 'apps.central'
