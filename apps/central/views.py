import random

from django.shortcuts import render, get_object_or_404, redirect
from django.views import View

from .models import (
    Topic,
    Test,
    Question,
    Answer,
    Result,
)


class TopicsList(View):

    def get(self, request):

        topics = Topic.objects.filter(show=True)
        return render(request, 'central/index.html', {'topics': topics})


class TopicView(View):

    def get(self, request, pk):
        topic = get_object_or_404(Topic, pk=pk)
        if not topic.show:
            return redirect('/')
        return render(request, 'central/topic.html', {'topic': topic})


class TestView(View):

    def get(self, request, pk):
        topic = get_object_or_404(Topic, pk=pk)
        test = Test.objects.get(topic=topic)
        questions = test.question_set.all()
        context = {'results': [], 'topic_id': pk}
        for q in questions:
            ans = list(q.answer_set.all())
            random.shuffle(ans)
            context['results'].append({'q': q, 'ans': ans})

        return render(request, 'central/test.html', context)

    def post(self, request, pk):
        data = request.POST
        print(data)
        topic = get_object_or_404(Topic, pk=pk)
        test = Test.objects.get(topic=topic)
        q_count = test.question_set.all().count()

        count = 0
        for d in data:
            try:
                index = int(d)
                ans = Answer.objects.get(pk=index)
                if ans.right:
                    count += 1
            except ValueError:
                pass

        try:
            Result.objects.update_or_create(user=request.user, topic=topic, defaults={'right_answers': count})
        except TypeError:
            pass

        return render(request, 'central/final.html', {'count': count, 'q_count': q_count,
                                                      't_id': topic.id})
